
public class Variablen2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		/* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
        Vereinbaren Sie eine geeignete Variable */
		
		byte Zaehler;
		
  /* 2. Weisen Sie dem Zaehler den Wert 25 zu
        und geben Sie ihn auf dem Bildschirm aus.*/

		Zaehler = 25;
		
  /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
        eines Programms ausgewaehlt werden.
        Vereinbaren Sie eine geeignete Variable */

		char Eingabe;
		
  /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
        und geben Sie ihn auf dem Bildschirm aus.*/

		Eingabe = 'C';
		System.out.println(Eingabe);
		
  /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
        notwendig.
        Vereinbaren Sie eine geeignete Variable */
		
		int Lichtgeschwindigkeit;
		
  /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
        und geben Sie sie auf dem Bildschirm aus.*/

		Lichtgeschwindigkeit = 300000;
		
  /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
        soll die Anzahl der Mitglieder erfasst werden.
        Vereinbaren Sie eine geeignete Variable und initialisieren sie
        diese sinnvoll.*/

		byte mitglieder;
		
  /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/

		mitglieder = 7;
		System.out.println("Anzahl der Mitglieder:" + mitglieder);
		
  /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
        Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
        dem Bildschirm aus.*/
		
		double elementarladung;
		elementarladung = 1.602176634E-19;
		System.out.println("Elementarladung: " + elementarladung + "C");

		
		
		
  /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
        Vereinbaren Sie eine geeignete Variable. */
		
		boolean zahlung;
		
  /*11. Die Zahlung ist erfolgt.
        Weisen Sie der Variable den entsprechenden Wert zu
        und geben Sie die Variable auf dem Bildschirm aus.*/
		
		
		zahlung = true;
		
		if (zahlung == true) System.out.println("Zahlung eingegangen");
		
		
		
	}

}
